package be.kdg.java2.demorequestparam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemorequestparamApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemorequestparamApplication.class, args);
    }

}
