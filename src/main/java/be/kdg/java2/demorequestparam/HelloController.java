package be.kdg.java2.demorequestparam;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Optional;

@Controller
public class HelloController {
   /* @GetMapping("/hello")
    public String sayHello(@RequestParam(name = "firstname", required = false) String firstname, Model model) {
        if (firstname == null) firstname = "John Doe";
        model.addAttribute("helloname", "Hello " + firstname);
        return "hello";
    }*/

    @GetMapping("/hello")
    public String sayHello(@RequestParam(name = "firstname") Optional<String> firstname, Model model) {
        String theFirstName = firstname.orElse("John Doe");
        model.addAttribute("helloname", "Hello " + theFirstName);
        return "hello";
    }
}
